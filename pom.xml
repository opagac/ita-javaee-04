<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>cz.ita.javaee</groupId>
	<artifactId>brainis</artifactId>
	<packaging>war</packaging>
	<version>1.0-SNAPSHOT</version>
	<name>brainis</name>

	<properties>
		<jdk.version>1.8</jdk.version>
		<spring.version>4.3.11.RELEASE</spring.version>
		<jackson.version>2.9.1</jackson.version>
		<logback.version>1.2.3</logback.version>
		<jcl.slf4j.version>1.7.25</jcl.slf4j.version>
		<servletapi.version>3.1.0</servletapi.version>
		<slf4j-api.version>1.7.25</slf4j-api.version>
		<guava.version>23.0</guava.version>
		<junit.version>4.12</junit.version>
		<hsqldb.version>2.4.0</hsqldb.version>
		<hamcrest-junit.version>2.0.0.0</hamcrest-junit.version>
		<slf4j-log4j12.version>1.7.25</slf4j-log4j12.version>
		<frontend-maven-plugin.version>1.6</frontend-maven-plugin.version>
		<wildfly-maven-plugin.version>1.2.0.Final</wildfly-maven-plugin.version>
		<maven-war-plugin.version>3.1.0</maven-war-plugin.version>
		<hibernate.version>5.2.11.Final</hibernate.version>
		<validation-api.version>2.0.0.Final</validation-api.version>
		<hibernate-validator.version>6.0.2.Final</hibernate-validator.version>
		<maven-compiler-plugin.version>3.7.0</maven-compiler-plugin.version>
		<jetty-maven-plugin.version>9.4.6.v20170531</jetty-maven-plugin.version>
		<nodejs.version>v8.5.0</nodejs.version>
		<npm.version>5.4.2</npm.version>
	</properties>

	<dependencies>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${spring.version}</version>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jdbc</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<!-- Need this for json to/from object -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson.version}</version>
		</dependency>

		<!-- Logging -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j-api.version}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>${jcl.slf4j.version}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4j-log4j12.version}</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>${logback.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-api</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Hibernate -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-core</artifactId>
			<version>${hibernate.version}</version>
		</dependency>

		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-entitymanager</artifactId>
			<version>${hibernate.version}</version>
		</dependency>

		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>9.4.1212</version>
		</dependency>

		<!-- jsr303 validation -->
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
			<version>${validation-api.version}</version>
		</dependency>

		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator</artifactId>
			<version>${hibernate-validator.version}</version>
		</dependency>

		<!--Testing-->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-junit</artifactId>
			<version>${hamcrest-junit.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${spring.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.hsqldb</groupId>
			<artifactId>hsqldb</artifactId>
			<version>${hsqldb.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- compile only, deployed container will provide this -->
		<!-- Need this for config annotation -->
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>${servletapi.version}</version>
			<scope>provided</scope>
		</dependency>

		<!--Utils-->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>${guava.version}</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${maven-compiler-plugin.version}</version>
				<configuration>
					<source>${jdk.version}</source>
					<target>${jdk.version}</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.tomcat.maven</groupId>
				<artifactId>tomcat7-maven-plugin</artifactId>
				<version>2.2</version>
				<configuration>
					<systemProperties>
						<spring.profiles.active>development</spring.profiles.active>
						<!--<JAVA_OPTS>-=development</JAVA_OPTS>-->
					</systemProperties>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.eclipse.jetty</groupId>
				<artifactId>jetty-maven-plugin</artifactId>
				<version>${jetty-maven-plugin.version}</version>
				<configuration>
					<scanIntervalSeconds>10</scanIntervalSeconds>
					<webApp>
						<contextPath>/</contextPath>
					</webApp>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>${maven-war-plugin.version}</version>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>

			<!-- Deploy to WildFly -->
			<plugin>
				<groupId>org.wildfly.plugins</groupId>
				<artifactId>wildfly-maven-plugin</artifactId>
				<version>${wildfly-maven-plugin.version}</version>
				<configuration>
					<hostname>127.0.0.1</hostname>
					<port>9990</port>
					<username>admin</username>
					<password>admin</password>
					<name>brainis.war</name>
					<java-opts>
						<java-opt>-Dspring.profiles.active=development</java-opt>
					</java-opts>
				</configuration>
			</plugin>

			<plugin>
				<groupId>com.github.eirslett</groupId>
				<artifactId>frontend-maven-plugin</artifactId>
				<version>${frontend-maven-plugin.version}</version>

				<configuration>
					<workingDirectory>src/main/webapp</workingDirectory>
				</configuration>

				<executions>
					<execution>
						<id>install node and npm</id>
						<goals>
							<goal>install-node-and-npm</goal>
						</goals>
						<phase>generate-resources</phase>
						<configuration>
							<nodeVersion>${nodejs.version}</nodeVersion>
							<npmVersion>${npm.version}</npmVersion>
						</configuration>
					</execution>
					<execution>
						<id>npm install</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<phase>generate-resources</phase>
						<configuration>
							<arguments>install</arguments>
						</configuration>
					</execution>
					<execution>
						<id>npm build</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<phase>generate-resources</phase>
						<configuration>
							<arguments>run build</arguments>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<artifactId>maven-war-plugin</artifactId>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
					<warSourceDirectory>${basedir}/src/main/webapp/dist</warSourceDirectory>
					<archive>
						<addMavenDescriptor>false</addMavenDescriptor>
					</archive>
					<!--<webResources>-->
						<!--<resource>-->
							<!--&lt;!&ndash; this is relative to the pom.xml directory &ndash;&gt;-->
							<!--<directory>${project.basedir}/src/main/resources</directory>-->
						<!--</resource>-->
					<!--</webResources>-->
					<packagingExcludes>
						WEB-INF/classes
					</packagingExcludes>
				</configuration>
			</plugin>

		</plugins>
	</build>
</project>