package cz.ita.javaee.repository.api;

import java.util.List;

import cz.ita.javaee.model.AbstractEntity;

public interface EntityRepository<T extends AbstractEntity> {
    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    T findOne(long id);

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @return list of founded entities
     */
    List<T> find(int offset, int limit);

    /**
     * Delete entity.
     * @param id identifier number of entity T
     */
    void delete(long id);

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    T update(final T entity);

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    T create(final T entity);
}
