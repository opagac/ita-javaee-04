package cz.ita.javaee.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController extends GenericController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "static/index.html";
	}

}
